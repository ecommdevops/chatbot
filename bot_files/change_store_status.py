import json, os, requests
from store_guid_lookup import store_guid_lookup

def change_store_status(store_id, sts = True):
    store_guid = store_guid_lookup(store_id)
    url = "https://api.redrooster.com.au/store/" + str(store_guid)
    headers = {
                "x-api-key": os.environ.get('store_api'),
                "Content-Type": "application/json"
            }
    
    payload = {
        "data": {
            "type": "store",
            "attributes": {
                "isEnabled": sts
            }
        }
    }

    str_payload = json.dumps(payload) 

    response = requests.request("PATCH", url, data=str_payload, headers=headers)
    json_response = json.loads(response.text)
    store_name = json_response['data']['attributes']['storeName']
    status_msg = 'on' if sts == True else 'off'

    if response.status_code == 200:
        return '{} turned {}!'.format(store_name, status_msg)
    else: 
        return 'Yor request was unsuccessful! {} {}'.format(store_guid, sts)

if __name__ == '__main__':
    print(change_store_status("284", True))

