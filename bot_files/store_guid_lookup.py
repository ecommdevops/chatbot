import json, requests, os

def store_guid_lookup(input_oheicsId):
    url = "https://api.redrooster.com.au/store/"

    querystring = {"include":"oheics"}

    payload = ""
    headers = {
        'x-api-key': os.environ.get('store_api'),
        'Content-Type': "application/json"
        }

    response = requests.request("GET", url, data=payload, headers=headers, params=querystring)

    all_stores = response.text

    all_stores_json = json.loads(all_stores)
    all_stores_list = all_stores_json['data']


    for store in all_stores_list:
        try:
            oheicsId = store['relationships']['oheics']['data'] ['attributes']['oheicsId']
            store_name = store['attributes']['storeName']
            store_guid = store['id']
            if str(oheicsId) == str(input_oheicsId):
                return (store_guid)
                exit()
            else:
                pass
        except Exception as e:
            return e


if __name__ == '__main__':
    print (store_guid_lookup(288884))