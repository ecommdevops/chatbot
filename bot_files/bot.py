# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

from botbuilder.core import ActivityHandler, TurnContext
from botbuilder.schema import ChannelAccount
from change_store_status import change_store_status


class MyBot(ActivityHandler):
    # See https://aka.ms/about-bot-activity-message to learn more about the message and other activity types.

    async def on_message_activity(self, turn_context: TurnContext):
        if 'turn on' in turn_context.activity.text:
            try:
                store_id = (turn_context.activity.text.split(' ')[-1]).strip()
            except Exception as e:
                pass
            await turn_context.send_activity(change_store_status(store_id))
        elif 'turn off' in turn_context.activity.text:
            try:
                store_id = (turn_context.activity.text.split(' ')[-1].strip())
            except Exception as e:
                pass
            await turn_context.send_activity(change_store_status(store_id, False))
        else:
            await turn_context.send_activity(f"You said '{ turn_context.activity.text }'")

    async def on_members_added_activity(
        self,
        members_added: ChannelAccount,
        turn_context: TurnContext
    ):
        for member_added in members_added:
            if member_added.id != turn_context.activity.recipient.id:
                await turn_context.send_activity("Hello and welcome!")


